const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('../api/routes');

const app = express();

const dbUsername = "ENTER USER NAME";
const dbPassword = "ENTER PASSWORD";

mongoose.connect('mongodb+srv://'+ dbUsername + ':' + dbPassword + 'dbURL', { useNewUrlParser: true, useUnifiedTopology: true }).
catch(error => console.error('Not connected', error));



app.use(cors());
app.use(bodyParser.urlencoded({extended: false})); //extended: false means that it supports simple bodies for url encoded data
app.use(bodyParser.json());


//assigning routes
app.use('/users', routes.users);
app.use('/trips', routes.trips);
// console.log('routes.users');



module.exports=app;