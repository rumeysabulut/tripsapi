const mongoose = require('mongoose');
const Schema = require('../schemas/trip');

const TripModel = mongoose.model('Trip', Schema);

module.exports = TripModel;