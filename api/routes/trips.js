require('dotenv').config();
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const tripModel = require('../models/trip');

router.get('/getByCircle', (req, res, next) => {
  if(req.headers.authorization) {
    const token = req.headers.authorization;
    const longitude = req.query.long;
    const latitude = req.query.lat;
    const radius = req.query.radius;
    const splitted = token.split('Bearer ');
    try {
      if(splitted.length > 1){
        const verifiedToken = jwt.verify(splitted[1], 'secret');
        tripModel.find({ start:
            { $geoWithin:
                { $centerSphere: [ [ longitude, latitude ], radius / 6378.1 ] } } }  ,function (err, trips) {
          if (err) return console.error(err);
          else if(trips) {
            res.status(200).send(trips);
          }

        })
      }
    } catch (e) {
      res.status(401).send('Token not verified');
    }
    

  }

});

module.exports = router;