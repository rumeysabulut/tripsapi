require('dotenv').config();
const express = require("express");
const router = express.Router();
const jwt = require('jsonwebtoken');
const Users = require('../models/users');

router.post('/login', (req, res, next) => {
  if (req.body && req.body.username && Users[req.body.username]) {
    const {username, password} = req.body;
    if (password === Users[username].password) {
      const userId = Users[username].id;
      const token = jwt.sign({
          username: Users[username].username,
          userId: Users[username].id
        }, 'secret');
      return res.status(200).json({
        message: "Auth successful",
        userId: userId,
        token: token
      });
    }
    res.status(401).json({
      msg : 'Passwords mismatch.',
    });
  } else {
    const msg = 'User not found!';
    console.warn(msg);
    res.status(401).send(msg);
  }
});


module.exports = router;