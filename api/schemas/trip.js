const mongoose = require('mongoose');
const tripSchema = new mongoose.Schema({
  distance_travelled: Number,
  year: Number,
  start: {
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  start_date: {type: Date},
  complete_date: {type: Date}
});

module.exports = tripSchema;